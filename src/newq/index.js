'use strict';

var _ = require('../_');
var globals = require('../globals');
var errors = require('./errors');

_.extend(module.exports, {
  // Methods & aliases
  all: require('./all'),
  any: require('./any'),
  bind: require('./bind'),
  defer: require('./defer'),
  each: require('./each'),
  filter: require('./filter'),
  map: require('./map'),
  join: require('./join'),
  method: require('./method'),
  props: require('./props'),
  reduce: require('./reduce'),
  reject: require('./reject'),
  resolve: require('./resolve'),
  some: require('./some'),
  when: require('./when'),

  // Errors
  AggregateError: errors.AggregateError,
  TimeoutError: errors.TimeoutError
});

if (globals.$options.compatibilityAliases) {
  _.extend(module.exports, {
    attempt: require('./try')
  });
}

if (!globals.$options.disableES5Methods) {
  _.extend(module.exports, {
    'try': require('./try')
  });
}
